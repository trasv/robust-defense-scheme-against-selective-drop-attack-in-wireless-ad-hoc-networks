# Robust Defense Scheme Against Selective Drop Attack in Wireless Ad Hoc Networks

> **Kelompok 7 :**<br/>
> M. Fatih&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(05111740000069)<br/>
> Bobbi Aditya&emsp;&emsp;&emsp;&emsp;(05111740000099)<br/>
> Samuel Marcellinus&emsp;(05111740000134)<br/>
> Bastian Farandy&emsp;&emsp;&ensp;&nbsp;(05111740000190)<br/>

<br/>

<a name="daftarisi"></a>
## Daftar Isi
- [Daftar Isi](#daftarisi)
- [1. Konsep](#konsep)
    *    [1.1 Deskripsi Paper](#1.1)
    *    [1.2 Latar Belakang](#1.2)
    *    [1.3 Dasar Teori](#1.3)
    *    [1.4 Routing Protokol yang Diusulkan](#1.4)
- [2. Implementasi](#implementasi)
    *    [2.1 Deskripsi Sistem](#2.1)
- [3. Testing](#testing)
- [4. Referensi](#referensi)
<br/>
<br/>

<a name="konsep"></a>
## 1. Konsep
<a name="1.1"></a>
### 1.1 Deskripsi Paper

Implementasi dari paper :<br/>
+ Judul : Robust Defense Scheme Against Selective Drop Attack in Wireless Ad Hoc Networks
+ Penulis : 
    -    T. POONGODI
    -    MOHAMMED S. KHAN
    -    RIZWAN PATAN
    -    AMIR H. GANDOMI
    -    BALAMURUGAN BALUSAMY
+ Terbit : 2019
+ Sumber : [Robust Defense Scheme Against Selective Drop Attack in Wireless Ad Hoc Networks](https://ieeexplore.ieee.org/document/8628956)


<a name="1.2"></a>
### 1.2 Latar Belakang

#### Permasalahan

<p>Performa dan keamanan merupakan 2 hal penting dalam wireless ad-hoc networks (WANETs). Keamanan pada jaringan diperlukan untuk memastikan integritas, ketersediaan dan performa dari WANETs. Hal ini membantu dalam mencegah interupsi dari service yang kritis dan meningkatkan produktifitas dengan menjaga jaringan untuk berjalan secara optimal. Karena di WANET tidak ada Centralized Network management, maka pada jaringan-jaringan ini dapat terkena serangan paket drop.</p>

#### Solusi

<p>Solusi yang ditawarkan untuk mencegah serangan packet drop pada paper ini adalah penggunaan skema resistive to selective drop attack (RSDA). Protokol RSDA ditujukan untuk mendeteksi node-node yang berbahaya di dalam jaringan ketika serangan drop terjadi. Protokol ini dapat diintegrasikan di banyak protocol routing yang sudah ada untuk WANETs seperti AODV dan DSR. Dalam mencapai reabilitas dalam routing dilakukan dengan menonaktifkan jalur dengan berat tertinggi dan mengotentikasi node menggunakan algoritma Curve Digital Signature.​</p>

<a name="1.3"></a>
### 1.3 Dasar Teori

#### AODV Secara Umum

<p>AODV Melakukan pengiriman data dari node sumber ke node tujuan dengan cara membroadcast untuk menentukan jalur tercepat dan menyimpan jalur tersebut selama dibutuhkan sehinggga pada saat pengiriman data tidak perlu lagi melakukan broadcast ulang, dan pada saat mengirim data akan melakukan pengecekan selalu di node – node tetangganya apakah masih dapat dilewati atau tidak​</p>
<p>AODV ini termasuk dalam klasifikasi reactive routing protocol, sebuah protokol yang hanya membuat rute ketika node sumber membutuhkannya. Ada 2 fase dalam AODV, yaitu route discovery dan route maintenance. ​</p>

#### Route Discovery

+ Route Discovery digunakan untuk meminta dan meneruskan informasi rute yang terdiri dari proses pengiriman Route Request (RREQ) dan Route Reply (RREP)
+ RREQ dibuat ketika ada node source yang ingin mengirimkan paket ke suatu node destination, dan node source tersebut masih belum memiliki route
+ Sedangkan RREP dibuat oleh node destination atau oleh node yang memiliki route menuju destination

#### Route Maintenance

Berupa Route Error (RERR)<br/>
![RERR](img/rerr.jpg "RERR")

#### Format RREQ dan RREP

![RERQ](img/rreq.jpg "RREQ")
![RERP](img/rrep.jpg "RREP")

#### Contoh Route Discovery

![Contoh Route Discovery](img/Contoh_Route_Discovery_.jpg "Contoh Route Discovery")

#### WANET (Wireless Ad-Hoc Network)

<p>WANET adalah suatu jaringan Ad Hoc yang terdapat dalam Jaringan Wireless pada perangkat komputer atau laptop.</p><br/>
![WANET](img/WANET.jpg "WANET")

<a name="1.4"></a>
### 1.4 Routing Protokol yang Diusulkan

#### Hypothetical Solution For RSDA Using ECDSA

+ ECDSA menawarkan tingkat keamanan yang sama seperti algoritma kryptografis lain dengan Panjang key yang lebih kecil​
+ Performa dapat meningkat dengan penggunaan daya dan bandwith yang dibutuhkan lebih sedikit​
+ Kurva eliptik dapat di definisikan dengan persamaan sebagai berikut:<br/>
![KurvaEliptik](img/kurvaeliptik.jpg "KurvaEliptik")

#### Curve Digital Signature Algorithm

##### Key Generation
![Key Generation](img/keygenerationprocess.jpg "Key Generation")
##### Signature Creation Process
![Signature Creation Process](img/signaturecreationprocess.jpg "Signature Creation Process")
##### Signatur Verification Process
![Signatur Verification Process](img/signatureverificationprocess.jpg "Signatur Verification Process")

#### Modified Protocol

+ Transmission rate akan di broadcast secara periodik oleh source dalam AREQ setelah di sign.
+ Jika Transmission rate naik/berkurang secara drastis (dibandingkan dengan bobot dan transmisson rate estimation) dan melibihi treshold value, maka dapat disimpulkan ada node yang bermasalah.​
+ Link yang menghubungkan node tersebut akan memutus rutenya, dan node tersebut berkewajiban mencari rute baru untuk menuju ke destinasi
+ Detection of Selective Drop Attack<br/>
![Detection of Selective Drop Attack​](img/DOSDA.jpg "Detection of Selective Drop Attack")

<a name="implementasi"></a>
## 2. Implementasi
<a name="2.1"></a>
### 2.1 Deskripsi Sistem
Implementasi routing protocol AODV modifikasi terhadap node berbahaya dilakukan pada sistem dengan:
+ Sistem operasi : linux mint 
+ Aplikasi yang digunakan
    + NS-2.35
<a name="testing"></a>
## 3. Testing
Percobaan dalam ns2 dengan beberapa metode menghasilkan nilai berikut ini<br/>![Hasil Percobaan](img/hasil.jpg "Hasil Percobaan")

Testing dilakukan dalam 9 skenario yang berbeda-beda. Dari 9 skenario tersebut, bisa di ambil 3 skenario umum yang berbeda, yakni :
+ Jaringan tanpa node berbahaya
+ Jaringan dengan node berbahaya tanpa pendeteksi
+ jaringan dengan node berbahaya dan node tersebut terdeteksi

File - file skenario testing bisa dilihat dalam folder testing 

<a name="referensi"></a>
## 4. Referensi

+ [Jenis Jaringan Ad Hoc](http://www.materi-it.com/2014/11/jenis-jaringan-ad-hoc.html)
+ [How the ECDSA algorithm works](https://kakaroto.homelinux.net/2012/01/how-the-ecdsa-algorithm-works/)
+ [ANALISIS KINERJA PROTOKOL ROUTING AD HOC ON-DEMAND DISTANCE VECTOR (AODV) PADA JARINGAN AD HOC HYBRID: PERBANDINGAN HASIL SIMULASI DENGAN NS-2 DAN IMPLEMENTASI PADA TESTBED DENGAN PDA](https://media.neliti.com/media/publications/149727-ID-analisis-kinerja-protokol-routing-ad-hoc.pdf)
+ [Modifikasi Route Discovery pada Ad-hoc on Demand Distance Vector (AODV) Berdasarkan Level Konektivitas One-Hop Node di VANETs](https://pdfs.semanticscholar.org/c9dc/760ee30ad8d3cfc67175e5a654c94da43817.pdf)
